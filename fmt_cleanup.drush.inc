<?php

/**
 * @file
 * Developed by Gabor Szanto.
 *  hello@szantogabor.com
 *  http://szantogabor.com
 */

/**
 * Implements hook_drush_command().
 */
function fmt_cleanup_drush_command() {
  $commands = [];

  $commands['fmt-cleanup'] = [
    'description' => dt('Cleanup file_managed table and file_system'),
    //'bootstrap' => ,
    'allow-additional-options' => TRUE,
    'options' => array(
      'size' => array(
        'description' => dt('The number of files to process in a single batch request.'),
        'example-value' => '500',
      ),
    ),
    'examples' => array(
      'drush fmt-cleanup --size=500' => dt('Will process 500 file in a row.'),
    ),
  ];

  $commands['fmt-status'] = [
    'description' => dt('Show the number of unused files.'),
  ];

  return $commands;
}

/**
 * Main command callback of drush fmt-cleanup.
 */
function drush_fmt_cleanup() {
  /** @var \Drupal\fmt_cleanup\Batch\Drush $service */
  $service = Drupal::service('fmt_cleanup.batch.drush');

  $service->setChunkSize(drush_get_option('size', 1000));
  $service->proceed();

  $batch = &batch_get();

  // Our terminal window doesn't need to know the progress of the operations
  $batch['progressive'] = FALSE;

  // Start processing the batch operations.
  drush_print(dt('Starting progress on @total files at @time.', [
    '@total' => $service::countUnusedFiles(),
    '@time' => Drupal::service('date.formatter')
      ->format(time(), 'custom', 'm/d - H:i:s'),
  ]));

  drush_backend_batch_process();
}

/**
 * Helper function to process a batch step.
 *
 * @param $batchsize
 *   How much files is deleted in one step.
 * @param $count
 *   The number of the step in the whole batch process.
 * @param $total
 *   The total number of the unused files.
 * @param $context
 *   The batch context.
 */
function drush_fmt_cleanup_process($batchsize, $count, $total, &$context) {
  /** @var \Drupal\fmt_cleanup\Batch\Drush $service */
  $service = Drupal::service('fmt_cleanup.batch.drush');
  $service::doStep($batchsize, $count, $total, $context);
}

/**
 * Main command callback for drush fmt-status.
 */
function drush_fmt_cleanup_fmt_status() {
  /** @var \Drupal\fmt_cleanup\Batch\Drush $service */
  $service = Drupal::service('fmt_cleanup.batch.drush');
  drush_print(dt('There are @total unused files.', ['@total' => $service->getUnusedFiles()]));
}

