<?php
/**
 * @file
 * Contains Drupal\fmt_cleanup\Batch.
 */


namespace Drupal\fmt_cleanup\Batch;


use Drupal\fmt_cleanup\FmtCleanupHelper;


class Drush extends FmtCleanupHelper {

  /**
   * {@inheritdoc}
   */
  public static function doStep($batchsize, $count, $total, &$context) {
    self::step($batchsize, $count, $total, $context);
  }

  /**
   * {@inheritdoc}
   */
  final static function step($batchsize, $count, $total, &$context) {
    // First, try resetting Drupal's static storage - this frequently releases
    // plenty of memory to continue.
    drupal_static_reset();

    // Entity storage can blow up with caches so clear them out.
    $manager = \Drupal::entityManager();
    foreach ($manager->getDefinitions() as $id => $definition) {
      $manager->getStorage($id)->resetCache();
    }

    // @TODO: explore resetting the container.

    // Run garbage collector to further reduce memory.
    gc_collect_cycles();
    parent::step($batchsize, $count, $total, $context);
    $context['message'] = self::getStepMessage($context);
  }

  /**
   * The batch finished callback.
   */
  protected static function doFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One file deleted.', '@count files deleted.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drush_print($message);
  }

  /**
   * The text of the message to show after a step is processed.
   *
   * @param array $context
   *   The batch context array.
   */
  protected static function getStepMessage($context) {
    return t('@time: @current files were processed, @remain left. Used memory: @memory', [
      '@current' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['total'],
      '@time' => self::$dateFormatter->format(time(), 'custom', 'm/d - H:i:s'),
      '@remain' => self::countUnusedFiles(),
      '@memory' => memory_get_usage(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getWorkerCallback() {
    return 'drush_fmt_cleanup_process';
  }

}
