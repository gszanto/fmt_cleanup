<?php
/**
 * @file
 * Contains Drupal\fmt_cleanup\Batch.
 */


namespace Drupal\fmt_cleanup\Batch;


use Drupal\fmt_cleanup\FmtCleanupHelper;

class FrontEnd extends FmtCleanupHelper{

  /**
   * {@inheritdoc}
   */
  public static function doStep($batchsize, $count, $total, &$context) {
    self::step($batchsize, $count, $total, $context);
  }

  /**
   * {@inheritdoc}
   */
  final static function step($batchsize, $count, $total, &$context) {
    parent::step($batchsize, $count, $total, $context);
    $context['message'] = self::getStepMessage($context);
  }

  /**
   * The text of the message to show after a step is processed.
   *
   * @param array $context
   *   The batch context array.
   */
  protected static function getStepMessage($context) {
    return t('@current files of @total were processed.', [
      '@current' => $context['sandbox']['progress'],
      '@total' => $context['sandbox']['total'],
    ]);
  }

  protected static function doFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One file processed.', '@count files processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function getWorkerCallback() {
    return [get_class($this), 'doStep'];
  }}
