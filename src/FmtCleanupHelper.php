<?php

namespace Drupal\fmt_cleanup;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class FmtCleanupHelper.
 */
abstract class FmtCleanupHelper implements FmtCleanupHelperInterface {

  use StringTranslationTrait;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected static $connection;

  /**
   * The date time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected static $dateTime;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected static $dateFormatter;

  /**
   * @var int
   */
  protected static $startedOn;

  /**
   * The procedure method of this entry.
   *
   * @var string
   */
  protected $chunkSize;

  /**
   * Constructs a new FmtCleanupHelper object.
   */
  public function __construct(Connection $connection, TimeInterface $datetime, DateFormatter $dateformatter) {
    self::$connection = $connection;
    self::$dateTime = $datetime;
    self::$dateFormatter = $dateformatter;
    self::$startedOn = $datetime->getCurrentTime();

    //ini_set('memory_limit', '256M');
  }

  /**
   * {@inheritdoc}
   */
  public function countAllFiles() {
    $return = self::$connection->query("SELECT COUNT(fid) FROM {file_managed}")
      ->fetchField();

    return $return ? $return : (int) 0;
  }

  /**
   * {@inheritdoc}
   */
  public function countUsedFiles($reset = FALSE) {
    $return = $this->countAllFiles() - $this->countUnusedFiles();
    return $return ? $return : (int) 0;
  }

  /**
   * {@inheritdoc}
   */
  public static function countUnusedFiles() {
    $return = self::$connection->query("SELECT COUNT(fm.fid) FROM {file_managed} fm LEFT JOIN {file_usage} fu ON fm.fid = fu.fid WHERE fu.fid IS NULL ORDER BY fm.fid")
      ->fetchField();

    return $return ? $return : (int) 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getChunkSize() {
    return $this->chunkSize;
  }

   /**
   * {@inheritdoc}
   */
  public function setChunkSize(int $size) {
    $this->chunkSize = $size;
  }

  /**
   * Initializa the batch.
   */
  private function batchSet() {
    $batch_size = $this->getChunkSize();
    $total = self::countUnusedFiles();
    $count = 0;
    $operations = [];
    $i = 0;
    $callback = $this->getWorkerCallback();

    while ($i <= $total) {
      $count++;
      $i = $i + $batch_size;
      $operations[] = [
        $callback,
        [
          $batch_size,
          $count,
          $total,
        ],
      ];
    }

    $batch = $this->getBatchDefaults();

    $batch['operations'] = $operations;

    batch_set($batch);
  }


  /**
   * Wrapper public function for batchSet.
   */
  public function proceed()  {
    $this->batchSet();
  }

  /**
   * The initial batch array.
   */
  private function getBatchDefaults() {
    return [
      'title' => $this->t('File cleanup batch'),
      'init_message' => $this->t('Starting file_managed table cleanup.'),
      'finished' => $this->getFinishedCallback(),
      'progress_message' => $this->t('Completed @current step of @total.'),
    ];
  }

  /**
   * Gets an array of file ids, which haven't any file_usage record.
   *
   * @param $chunkSize
   *   The number files.
   */
  private static function getFids($chunkSize) {
    $return = [];
    $query = self::$connection->select('file_managed', 'fm');
    $query->leftJoin('file_usage', 'fu', 'fm.fid = fu.fid');
    $query->addField('fm', 'fid');
    $query->isNull('fu.fid');
    $query->range(0, $chunkSize);

    $a = $query->execute();
    $a =$a->fetchAllAssoc('fid');

    foreach ($query->execute()->fetchAllAssoc('fid') as $item) {
      $return[] = $item->fid;
    }

    return $return;
  }

  /**
   * The core function of the batch, does the cleanup.
   */
  protected static function step($batchsize, $count, $total, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_number'] = 0;
      $context['sandbox']['total'] = $total;
    }

    foreach (self::getFids($batchsize) as $fid) {
      $e = memory_get_usage();
      file_delete($fid);
      $context['sandbox']['progress']++;
      $context['sandbox']['current_number'] = $count;
    }

    //file_delete_multiple(self::getFids($batchsize));

    if ($context['sandbox']['progress'] != $context['sandbox']['total']) {
      $context['finished'] = $context['sandbox']['progress'] / $total;
    }
  }

  /**
   * Set up the worker callback, which does the actual step.
   *
   * @return array|string
   *  If the return value is an array, like ['class', 'method'] the callback
   *  will be class::method. If it is a string, it will be a procedural
   *  function.
   */
  protected function getWorkerCallback() {}

  /**
   * Set up the bach finished callback.
   *
   * @return array|string
   *  If the return value is an array, like ['class', 'method'] the callback
   *  will be class::method. If it is a string, it will be a procedural
   *  function.
   */
  protected function getFinishedCallback() {
    return [get_class($this), 'doFinished'];
  }

  /**
   * The wrapper public method of a step.
   */
  public static function doStep($batchsize, $count, $total, &$context) {}
}

