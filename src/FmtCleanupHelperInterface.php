<?php

namespace Drupal\fmt_cleanup;

/**
 * Interface FmtCleanupHelperInterface.
 */
interface FmtCleanupHelperInterface {

  /**
   * Get the number all managed files.
   *
   * @return int
   *   The number of files.
   */
  public function countAllFiles();

  /**
   * Get the number of files based in the file_managed table which aren't any
   * file usage record.
   *
   * @return int
   *   The number of unused iles.
   */
  public static function countUnusedFiles();

  /**
   * Get the number of files based in the file_managed table which have
   * file_usage record.
   *
   * @return int
   *   The number of used files.
   */
  public function countUsedFiles();

  /**
   * Get how much file items to be processed in one request.
   */
  public function getChunkSize();

  /**
   * Set how much file items to be processed in one request.
   */
  public function setChunkSize(int $size);

}
