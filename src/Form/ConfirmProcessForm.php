<?php

namespace Drupal\fmt_cleanup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\fmt_cleanup\Batch\FrontEnd;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfirmProcessForm.
 */
class ConfirmProcessForm extends FormBase {

  /**
   * @var \Drupal\fmt_cleanup\FmtCleanupHelper
   */
  protected $fmtCleanupHelper;

  /**
   * Constructs a new SettingsForm object.
   */
  public function __construct(FrontEnd $helper) {
    $this->fmtCleanupHelper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fmt_cleanup.batch.frontend')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fmt_cleanup_confirm_process_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $all = $this->fmtCleanupHelper->countAllFiles();
    $used = $this->fmtCleanupHelper->countUsedFiles();
    $unused = $this->fmtCleanupHelper->countUnusedFiles();

    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Files per chunk'),
      '#description' => $this->t('How many files to be processed in one request.'),
      '#default_value' => 2000,
    ];

    // @todo: Create queue handler.
    /**
    $form['use_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use queue'),
      '#description' => $this->t('If checked, unused files will be deleted by queue instead of batch.'),
      '#default_value' => FALSE,
    ];
    */

    $form['details'] = [
      '#type' => 'container',
      '#title' => $this->t('Are you sure?'),
    ];

    $form['details']['markup'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('There are only @used managed files of @all. This will remove @unused files.', ['@used' => $used, '@all' => $all, '@unused' => $unused]),
    ];


    $form['details']['warning'] = [
      '#prefix' => '<div class="messages messages--warning">',
      '#suffix' => '</div>',
      '#markup' => $this->t('This action cannot be undone!')
    ];

    $form['proceed'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start cleanup'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->fmtCleanupHelper->setChunkSize($form_state->getValue('batch_size'));
    $this->fmtCleanupHelper->proceed();
  }

  /**
   * Page title callback for the config form.
   */
  public function getPageTitle() {
    return $this->t('Are you sure to delete @total items of the file_managed table?', ['@total' => $this->fmtCleanupHelper->countUnusedFiles()]);
  }
}
