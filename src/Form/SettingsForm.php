<?php

namespace Drupal\fmt_cleanup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fmt_cleanup_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['make_unused_managed_files_temporary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Makes unused managed files temporary'),
      '#description' => $this->t('If checked, unused files will be deleted automatically.'),
      '#default_value' => $this->config('file.settings')->get('make_unused_managed_files_temporary'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Proceed to cleanup'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $form_state->setRedirect('fmt_cleanup.confirm_process_form');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      if ($key == 'make_unused_managed_files_temporary') {
        $this->configFactory->getEditable('file.settings')
          ->set('make_unused_managed_files_temporary', $value)->save();
      }
    }

  }

}
